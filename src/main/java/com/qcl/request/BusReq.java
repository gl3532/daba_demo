package com.qcl.request;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 */
@Data
public class BusReq {
    private Integer BusId;
    @NotEmpty(message = "大巴编号必填")
    private String busName;
    @NotNull(message = "票价必填")
    private BigDecimal busPrice;
    private Integer busStock;
    private String busDesc;
//    @NotEmpty(message = "菜品图必填")
//    private String busIcon;

    private Integer leimuType;
}
