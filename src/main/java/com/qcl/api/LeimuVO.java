package com.qcl.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 大巴(包含类目)

 */
@Data
public class LeimuVO {

    @JsonProperty("name")
    private String leimuName;

    @JsonProperty("type")
    private Integer leimuType;

    @JsonProperty("buses")
    private List<BusRes> busResList;
}
