package com.qcl.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

//import com.qcl.meiju.BusStatusEnum;
import lombok.Data;

/**
 * 返回给小程序的大巴页
 */
@Data
public class BusRes {
    @JsonProperty("id")
    private Integer busId;

    @JsonProperty("name")
    private String busName;

    @JsonProperty("price")
    private BigDecimal busPrice;
    @JsonProperty("stock")
    private Integer busStock;//库存

    @JsonProperty("desc")
    private String busDesc;


}
