package com.qcl.handler;

import com.qcl.yichang.DaBaAuthorizeException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 */
@ControllerAdvice
public class DaBaExceptionHandler {


    //拦截登录异常
    //http://localhost:8080/daba/leimu/list
    @ExceptionHandler(value = DaBaAuthorizeException.class)
    public String handlerAuthorizeException() {
        return "zujian/loginView";
    }
}
