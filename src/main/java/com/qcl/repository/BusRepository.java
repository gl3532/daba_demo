package com.qcl.repository;

import com.qcl.bean.Bus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * 菜品repository
 */
public interface BusRepository extends JpaRepository<Bus, Integer> {

    List<Bus> findByBusStockLessThan(int num);//查询库存少于num的

    List<Bus> findByBusStatusAndBusNameContaining(Integer busStatus, String name);

    List<Bus> findByBusStatus(Integer busStatus);

}
