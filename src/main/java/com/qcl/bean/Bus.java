package com.qcl.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qcl.meiju.BusStatusEnum;
import com.qcl.utils.EnumUtil;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 大巴属性
 */
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class Bus {
    @Id
    @GeneratedValue
    private Integer busId;//id
    private String busName;//大巴名
    private BigDecimal busPrice;//大巴单价
    private Integer busStock;//库存
    private String busDesc;//大巴描述
    private Integer busStatus = BusStatusEnum.UP.getCode();//状态, 0正常1下架.

    private Integer leimuType;//大巴类目编号
    private Integer adminId;//大巴属于那个商家

    @CreatedDate//自动添加创建时间的注解
    private Date createTime;
    @LastModifiedDate//自动添加更新时间的注解
    private Date updateTime;

    @JsonIgnore
    public BusStatusEnum getBusStatusEnum() {
        return EnumUtil.getByCode(busStatus, BusStatusEnum.class);
    }


}
