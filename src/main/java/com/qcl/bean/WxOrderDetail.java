package com.qcl.bean;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * 用户订单--订单详情
 */
@Entity
@Data
public class WxOrderDetail {
    @Id
    @GeneratedValue
    private Integer detailId;
    private Integer orderId;//订单id.
    private int busId;//大巴id
    private String busName;
    private BigDecimal busPrice;
    private Integer busQuantity;//下单数量
//    private String busIcon;//商品图
}
