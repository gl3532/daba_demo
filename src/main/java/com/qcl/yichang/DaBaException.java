package com.qcl.yichang;

import com.qcl.meiju.ResultEnum;

/**
 * 编程小石头：2501902696（微信）
 */
public class DaBaException extends RuntimeException{

    private Integer code;

    public DaBaException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());

        this.code = resultEnum.getCode();
    }

    public DaBaException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
