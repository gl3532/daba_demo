package com.qcl.controller;

import com.qcl.api.ResultVO;
import com.qcl.bean.PictureInfo;
import com.qcl.repository.PictureRepository;
import com.qcl.utils.ApiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 小程序端轮播图
 */
@RestController
@RequestMapping("/wxPicture")
public class WxPictureController {
    @Autowired
    PictureRepository repository;

    /*
     * 返回json给小程序
     * */
    @GetMapping("/getAll")
    @ResponseBody
    public ResultVO getUserInfo() {
        List<PictureInfo> pictures = repository.findAll();
        return ApiUtil.success(pictures);
    }

}
