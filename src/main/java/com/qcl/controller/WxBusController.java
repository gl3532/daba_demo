package com.qcl.controller;

import com.qcl.api.BusRes;
import com.qcl.api.LeimuVO;
import com.qcl.api.ResultVO;
import com.qcl.bean.Bus;
import com.qcl.bean.Leimu;
import com.qcl.meiju.BusStatusEnum;
import com.qcl.repository.BusRepository;
import com.qcl.repository.LeiMuRepository;
import com.qcl.utils.ApiUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 小程序买家端,大巴列表
 */
@RestController
@Slf4j
public class WxBusController {

    @Autowired
    private BusRepository busRepository;

    @Autowired
    private LeiMuRepository leiMuRepository;

    /*
     * 返回菜单和菜品列表
     * */
    @GetMapping("/buyerbusList")
    public ResultVO list(@RequestParam("searchKey") String searchKey) {
        log.info("搜索词={}", searchKey);
        List<Bus> busList = new ArrayList<>();
        if (StringUtils.pathEquals("all", searchKey)) {
            //返回所有菜品
            busList = busRepository.findByBusStatus(BusStatusEnum.UP.getCode());
        } else {
            //查询菜品
            busList = busRepository.findByBusStatusAndBusNameContaining(BusStatusEnum.UP.getCode(), searchKey);
            log.info("搜索结果={}", busList);
        }

        return zuZhuang(busList);
    }

    public ResultVO zuZhuang(List<Bus> busList) {
        List<Integer> categoryTypeList = busList.stream()
                .map(e -> e.getLeimuType())
                .collect(Collectors.toList());
        List<Leimu> leimuList = leiMuRepository.findByLeimuTypeIn(categoryTypeList);

        //3. 数据拼装
        List<LeimuVO> leimuVOList = new ArrayList<>();
        for (Leimu leimu : leimuList) {
            LeimuVO leimuVO = new LeimuVO();
            leimuVO.setLeimuType(leimu.getLeimuType());
            leimuVO.setLeimuName(leimu.getLeimuName());

            List<BusRes> busResList = new ArrayList<>();
            for (Bus bus : busList) {
                if (bus.getLeimuType().equals(leimu.getLeimuType())) {
                    BusRes busRes = new BusRes();
                    BeanUtils.copyProperties(bus, busRes);
                    busResList.add(busRes);
                }
            }
            leimuVO.setBusResList(busResList);
            leimuVOList.add(leimuVO);
        }

        return ApiUtil.success(leimuVOList);
    }
}
