package com.qcl.controller;

import com.qcl.bean.Bus;
import com.qcl.bean.Leimu;
import com.qcl.meiju.BusStatusEnum;
import com.qcl.meiju.ResultEnum;
import com.qcl.repository.BusRepository;
import com.qcl.repository.LeiMuRepository;
import com.qcl.request.BusReq;
import com.qcl.utils.ExcelExportUtils;
import com.qcl.utils.ExcelImportUtils;
import com.qcl.yichang.DaBaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * 大巴管理
 */
@Controller
@RequestMapping("/bus")
@Slf4j
public class AdminBusController {

    @Autowired
    private BusRepository busRepository;

    @Autowired
    private LeiMuRepository leiMuRepository;

    //列表
    @GetMapping("/list")
    public String list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size,
                       ModelMap map) {
        PageRequest request = PageRequest.of(page - 1, size);
        Page<Bus> busPage = busRepository.findAll(request);
        map.put("busPage", busPage);
        map.put("currentPage", page);
        map.put("size", size);
        return "bus/list";
    }

    //删除某个
    @GetMapping("/remove")
    public String remove(@RequestParam(value = "busId", required = false) Integer busId,
                         ModelMap map) {
        busRepository.deleteById(busId);
        map.put("url", "/daba/bus/list");
        return "zujian/success";
    }

    //详情页
    @GetMapping("/index")
    public String index(@RequestParam(value = "busId", required = false) Integer busId,
                        ModelMap map) {
        if (busId != null) {
            Bus bus = busRepository.findById(busId).orElse(null);
            map.put("bus", bus);
        }
        //查询所有的类目
        List<Leimu> leimuList = leiMuRepository.findAll();
        map.put("leimuList", leimuList);
        return "bus/index";
    }

    //上架
    @RequestMapping("/on_sale")
    public String onSale(@RequestParam("busId") int busId,
                         ModelMap map) {
        try {
            Bus bus = busRepository.findById(busId).orElse(null);
            if (bus == null) {
                throw new DaBaException(ResultEnum.PRODUCT_NOT_EXIST);
            }
            if (bus.getBusStatusEnum() == BusStatusEnum.UP) {
                throw new DaBaException(ResultEnum.PRODUCT_STATUS_ERROR);
            }
            bus.setBusStatus(BusStatusEnum.UP.getCode());
            busRepository.save(bus);
        } catch (DaBaException e) {
            map.put("msg", e.getMessage());
            map.put("url", "/daba/bus/list");
            return "zujian/error";
        }

        map.put("url", "/daba/bus/list");
        return "zujian/success";
    }

    //下架
    @RequestMapping("/off_sale")
    public String offSale(@RequestParam("busId") int busId,
                          ModelMap map) {
        try {
            Bus bus = busRepository.findById(busId).orElse(null);
            if (bus == null) {
                throw new DaBaException(ResultEnum.PRODUCT_NOT_EXIST);
            }
            if (bus.getBusStatusEnum() == BusStatusEnum.DOWN) {
                throw new DaBaException(ResultEnum.PRODUCT_STATUS_ERROR);
            }
            bus.setBusStatus(BusStatusEnum.DOWN.getCode());
            busRepository.save(bus);
        } catch (DaBaException e) {
            map.put("msg", e.getMessage());
            map.put("url", "/daba/bus/list");
            return "zujian/error";
        }

        map.put("url", "/daba/bus/list");
        return "zujian/success";
    }


    //添加或更新
    @PostMapping("/save")
    public String save(@Valid BusReq form,
                       BindingResult bindingResult,
                       ModelMap map) {
        if (bindingResult.hasErrors()) {
            map.put("msg", bindingResult.getFieldError().getDefaultMessage());
            map.put("url", "/daba/bus/index");
            return "zujian/error";
        }

        Bus productInfo = new Bus();
        try {
            //如果productId为空, 说明是新增
            if (!StringUtils.isEmpty(form.getBusId())) {
                productInfo = busRepository.findById(form.getBusId()).orElse(null);
            }
            BeanUtils.copyProperties(form, productInfo);
            busRepository.save(productInfo);
        } catch (Exception e) {
            log.error("添加错误={}", e);
            map.put("msg", "添加出错");
            map.put("url", "/daba/bus/index");
            return "zujian/error";
        }

        map.put("url", "/daba/bus/list");
        return "zujian/success";
    }


    //导出菜品到excel
    @GetMapping("/export")
    public String export(HttpServletResponse response, ModelMap map) {
        String fileName = "商品导出";
        String[] titles = {"大巴编号", "票价", "库存", "类目", "描述", };
        List<Bus> busList = busRepository.findAll();
        if (busList == null || busList.size() < 1) {
            map.put("msg", "大巴为空");
            map.put("url", "/daba/bus/list");
            return "zujian/error";
        }
        int size = busList.size();
        String[][] dataList = new String[size][titles.length];
        for (int i = 0; i < size; i++) {
            Bus bus = busList.get(i);
            dataList[i][0] = bus.getBusName();
            dataList[i][1] = "" + bus.getBusPrice();
            dataList[i][2] = "" + bus.getBusStock();//库存
            dataList[i][3] = "" + bus.getLeimuType();//类目的type
            dataList[i][4] = bus.getBusDesc();
//            dataList[i][5] = bus.getBusIcon();
        }

        try {
            ExcelExportUtils.createWorkbook(fileName, titles, dataList, response);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("msg", "导出excel失败");
            map.put("url", "/daba/bus/list");
            return "zujian/error";
        }
        map.put("url", "/daba/bus/list");
        return "zujian/success";
    }

    //excel导入网页
    @GetMapping("/excel")
    public String excel(ModelMap map) {
        return "bus/excel";
    }

    /*
     * 批量导入excel里的大巴到数据库
     * */
    @RequestMapping("/uploadExcel")
    public String uploadExcel(@RequestParam("file") MultipartFile file,
                              ModelMap map) {
        String name = file.getOriginalFilename();
        if (name.length() < 6 || !name.substring(name.length() - 5).equals(".xlsx")) {
            map.put("msg", "文件格式错误");
            map.put("url", "/daba/bus/excel");
            return "zujian/error";
        }
        List<Bus> list;
        try {
            list = ExcelImportUtils.excelToBusInfoList(file.getInputStream());
            log.info("excel导入的list={}", list);
            if (list == null || list.size() <= 0) {
                map.put("msg", "导入失败");
                map.put("url", "/daba/bus/excel");
                return "zujian/error";
            }
            //excel的数据保存到数据库
            try {
                for (Bus excel : list) {
                    if (excel != null) {
                        busRepository.save(excel);
                    }
                }
            } catch (Exception e) {
                log.error("某一行存入数据库失败={}", e);
            }

        } catch (Exception e) {
            e.printStackTrace();
            map.put("msg", e.getMessage());
            map.put("url", "/daba/bus/excel");
            return "zujian/error";
        }
        map.put("url", "/daba/bus/list");
        return "zujian/success";
    }


}
