package com.qcl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class DaBaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DaBaApplication.class, args);
	}
}
